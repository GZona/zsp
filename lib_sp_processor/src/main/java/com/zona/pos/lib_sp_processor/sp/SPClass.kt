package com.zona.pos.lib_sp_processor.sp


import java.lang.StringBuilder


/**
 * @ClassName SPClass
 * @Description
 * @Author zona
 * @Date 2021/4/1 10:19
 * @Version 1.0
 */
class SPClass(
        val packageName: String,
        val iClassName: String,
        val className: String,
        val spFileName: String,
) {


    /**
     * 导入类
     */
    private val importList = ArrayList<String>()

    val fieldList = HashMap<String, SpField>()

    /**
     * 创建SP的全局属性
     * @param fieldClassName 参数的类名
     * @param fieldName 参数名
     * @param default 默认值
     * @param keyName 键值
     * @param isOverride 是否是继承关系
     */
    fun crateSpField(
            fieldClassName: String,
            fieldName: String,
            default: String,
            keyName: String,
            isOverride: Boolean
    ) {
        val list = ArrayList<String>()
        getT(list, fieldClassName)
        var className = fieldClassName
        list.forEach { name ->
            name.split(",").forEach { fieldClassName ->
                val endIndex = fieldClassName.indexOfLast { it == '.' }
                if (endIndex > 0) {
                    var newClass = addImportList(fieldClassName)
                    if (newClass == "Integer") {
                        newClass = "Int"
                    }
                    className = className.replace(fieldClassName, newClass)
                }
            }
        }
//        var className = fieldClassName
//        val endIndex = fieldClassName.indexOfLast { it == '.' }
//        if (endIndex > 0) {
//            className = "${fieldClassName.subSequence(endIndex + 1, fieldClassName.length)}"
//            val importName = "${fieldClassName.subSequence(0, endIndex + 1)}*"
//            if (!importList.contains(importName)) {
//                importList.add(importName)
//            }
//        }
        System.out.println("------------------>fieldName:${fieldName}  crateSpField1:${default}   className:${className}")
        fieldList[fieldName] = SpField(
                className,
                fieldName = fieldName,
                default,
                keyName,
                isOverride
        )
    }

    /**
     * 将含有泛型的类名分离出来
     */
    private fun getT(list: ArrayList<String>, className: String) {
        val startIndex = className.indexOfFirst { it == '<' }
        val endIndex = className.indexOfLast { it == '>' }
        if (startIndex > 0) {
            list.add(className.subSequence(0, startIndex).toString())
            getT(list, className.subSequence(startIndex + 1, endIndex).toString())
            if (endIndex < className.length) {
                getT(list, className.subSequence(startIndex + 1, endIndex).toString())
            }
        } else {
            list.add(className)
        }

    }

    private fun addImportList(fieldClassName: String): String {
        val endIndex = fieldClassName.indexOfLast { it == '.' }
        if (endIndex > 0) {
            val importName = "${fieldClassName.subSequence(0, endIndex + 1)}*"
            if (!importList.contains(importName)) {
                importList.add(importName)
            }
            return "${fieldClassName.subSequence(endIndex + 1, fieldClassName.length)}"
        }
        return fieldClassName
    }

    fun createClassStart(s: StringBuilder) {
        s.append("package ${packageName}\n")
        s.append("import com.zona.lib_sp.* \n")
        s.append("import com.google.gson.reflect.TypeToken \n")
        s.append("import com.zona.lib_sp_annotation.* \n")
        getSpPackageName()?.let { s.append("import ${it}.* \n") }
        s.append("import kotlin.properties.* \n")
        importList.forEach {
            s.append("import ${it}\n")
        }
        s.append("class $className :${iClassName}{\n  ")
    }

    fun createClassEnd(s: StringBuilder) {
        s.append("}\n  ")
    }

    private fun getSpPackageName(): String? {
        Class.forName("com.zona.lib_sp.Constant").declaredFields.forEach {
            if (it.name == "SP_PACKAGE_NAME") {
                return it.get(it).toString()
            }
        }
        return null
    }


    fun createFieldString(spField: SpField, s: StringBuilder): String {

        val newFieldName = spField.fieldName + "_d"
        val spFieldName = spField.fieldName + "_sp"
        val spAFieldName = spField.fieldName + "_sp_a"
        val baseClassType = getKotlinBaseClassTypeType(spField.className)

        var default = ""
        var spClassType = ""
        var dClassType = ""
        when (baseClassType) {
            null -> {
                spClassType = "String"
                dClassType = if (spField.className == "String") {
                    "String"
                } else {
                    "${spField.className}?"
                }
                default = "\"${spField.default}\""
            }
            else -> {
                spClassType = baseClassType
                dClassType = baseClassType
                default = getDefault(spField.className, spField.default)
            }
        }

        var initialValue = ""
        var newValue = ""
        if (baseClassType != null || spField.className == "String") {
            initialValue = spFieldName
            newValue = "newValue"

            s.append("  private var ${spFieldName}: $spClassType by SharedPref( default = ${default}, keyName = \"${spField.keyName}\", spName = \"${spFileName}\" )\n")
            s.append("\n")
            s.append(
                "  private var $newFieldName by Delegates.observable(${initialValue}) { _, _, newValue ->\n" +
                        "        $spFieldName = ${newValue}\n" +
                        "    }\n"
            )
        } else {
            initialValue =
                    "SpUtils.deSerialization<${spField.className}>(${spFieldName},${spAFieldName},object : TypeToken<${spField.className}>() {}.getType())"
            newValue = "SpUtils.serialize(\"${spField.fieldName}\",newValue,object :TypeToken<${spField.className}>() {})"

            s.append("  private var ${spAFieldName}: String by SharedPref( default = \"\", keyName = \"${spAFieldName}\", spName = \"${spFileName}\" )\n")

            s.append("  private var ${spFieldName}: $spClassType by SharedPref( default = ${default}, keyName = \"${spField.keyName}\", spName = \"${spFileName}\" )\n")
            s.append("\n")
            s.append(
                "  private var $newFieldName by Delegates.observable(${initialValue}) { _, _, newValue ->\n" +
                        "        val spData = ${newValue}\n" +
                        "        if(spData==null){\n" +
                        "           $spFieldName = \"\"\n" +
                        "           $spAFieldName = \"\"\n" +
                        "        }else{\n" +
                        "           $spFieldName = spData.json\n" +
                        "           $spAFieldName  = spData.clazzList\n" +
                        "        }\n" +
//                        "        $spFieldName = ${newValue}\n" +
                        "    }\n"
            )
        }


        s.append("\n")
        s.append("    ${if (!spField.isOverride) "" else "override"} var ${spField.fieldName}: ${dClassType}\n")
        s.append("        get() = ${newFieldName}\n")
        s.append("        set(value) {\n")
        s.append("            $newFieldName = value\n")
        s.append("    }\n")
        s.append("\n")
        return s.toString()
    }


    fun getDefault(classType: String, default: String): String {
        if (default.isNotEmpty()) {
            return when (classType) {
                "int" -> default
                "double" -> {
                    if (default.toIntOrNull()?.toDouble() == default.toDoubleOrNull()) {
                        "${default.toIntOrNull()}.0"
                    } else default
                }
                "float" -> "${default}f"
                "byte" -> default
                "short" -> default
                "long" -> "${default}L"
                "char" -> "\'${default}\'"
                "boolean" -> default
                else -> ""
            }
        } else {
            return when (classType) {
                "int" -> "0"
                "float" -> "0f"
                "double" -> "0.0"
                "byte" -> "0"
                "short" -> "0"
                "long" -> "0L"
                "char" -> "0"
                "boolean" -> "false"
                else -> ""
            }
        }
    }

    fun getKotlinBaseClassTypeType(classType: String): String? {
        return when (classType) {
            "int" -> "Int"
            "float" -> "Float"
            "double" -> "Double"
            "byte" -> "Byte"
            "short" -> "Short"
            "long" -> "Long"
            "char" -> "Char"
            "boolean" -> "Boolean"
            else -> null
        }
    }

}