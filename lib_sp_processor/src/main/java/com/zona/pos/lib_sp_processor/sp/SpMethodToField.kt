package com.zona.pos.lib_sp_processor.sp


/**
 * @ClassName SpMethodToField
 * @Description 将get  set 方法转换为全局变量
 * @Author zona
 * @Date 2021/4/1 17:15
 * @Version 1.0
 */
class SpMethodToField {
    var setField: SetField? = null
    var getField: GetField? = null

    /**
     * 默认值
     */
    var default: String = ""

    /**
     * 基础变量
     */
    var keyName: String = ""

    fun createSetField(parameter: String, className: String) {
        setField = SetField(parameter, className)
        keyName = parameter
    }

    fun createGetField(key:String,returnType: String, className: String) {
        getField = GetField(key,returnType, className)
        keyName = key
    }

    class SetField(val parameter: String, val className: String) {

    }

    class GetField(val key:String,val returnType: String, val className: String) {

    }
}