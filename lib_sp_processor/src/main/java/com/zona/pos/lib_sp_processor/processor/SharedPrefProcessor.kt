package com.zona.pos.lib_sp_processor.processor

import com.google.auto.service.AutoService
import com.zona.lib_sp_annotation.AnSharedPref
import com.zona.lib_sp_annotation.AnSpField
import com.zona.pos.lib_sp_processor.sp.SPClass
import com.zona.pos.lib_sp_processor.sp.SpMethodToField
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.io.Writer
import java.util.*
import javax.annotation.processing.ProcessingEnvironment
import javax.annotation.processing.Processor
import javax.annotation.processing.RoundEnvironment
import javax.lang.model.element.*
import javax.tools.StandardLocation
import kotlin.collections.HashMap
import kotlin.collections.LinkedHashSet


/**
 * @ClassName SharedPrefProcessor
 * @Description 注冊监听Sp
 * @Author zona
 * @Date 2021/3/31 15:40
 * @Version 1.0
 */
@AutoService(Processor::class)
class SharedPrefProcessor : BaseProcessor() {

    private var generated: String? = null

    override fun init(processingEnv: ProcessingEnvironment?) {
        super.init(processingEnv)

        val options = processingEnv?.options
        generated = options?.get("kapt.kotlin.generated")
//        log("options:${options}")
    }

    override fun process(
        annotations: MutableSet<out TypeElement>?,
        roundEnv: RoundEnvironment?
    ): Boolean {
        if (annotations != null && annotations.isNotEmpty()) {
            roundEnv?.getElementsAnnotatedWith(
                AnSharedPref::class.java
            )?.also { anSharedPrefElements ->
                try {
                    this.parseAnSharedPref(anSharedPrefElements, roundEnv)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                return true
            }
        }

        return true
    }

    override fun getSupportedAnnotationTypes(): MutableSet<String> {
        val types: MutableSet<String> = LinkedHashSet()
        types.add(AnSharedPref::class.java.canonicalName)
        return types
    }

    private fun parseAnSharedPref(anSharedPrefElements: Set<Element>, roundEnv: RoundEnvironment) {
        anSharedPrefElements.forEach { element ->
//            log("element:${element}")
            if (element.kind == ElementKind.INTERFACE) {//接口
                val ie = element as TypeElement
                //创建Sp类文件所需要的完成数据
                val spClass = createSPClass(ie)
                ////收集set get 方法信息
                val spMethodToFieldMap = HashMap<String, SpMethodToField>()
                //缓存set get方法的注解信息
                val anSpFieldMap = HashMap<String, AnSpField>()
                element.enclosedElements?.forEach { enclosedElements ->
//                    log("enclosedElements:${enclosedElements}")
                    when (enclosedElements.kind) {
                        ElementKind.FIELD -> {
                            //添加全局变量
                            addSpField(enclosedElements as VariableElement, spClass)
                        }
                        ElementKind.METHOD -> {
                            //收集set get 方法信息，为了将该方法融合为全局变量做准备
                            addSpMethodToField(
                                enclosedElements as ExecutableElement,
                                spMethodToFieldMap
                            )
                        }
                        ElementKind.CLASS -> {
                            //kotlin的特殊属性，该属性中缓存了接口中全局变量、方法、注解的默认值
                            enclosedElements.enclosedElements?.forEach { e ->
//                                log("e:${e}")
                                if (e.kind == ElementKind.METHOD) {
                                    val ee = e as ExecutableElement
//                                    log("ee:${ee}")
                                    e.getAnnotation(AnSpField::class.java)
                                        ?.let { anSpField ->
//                                                log("anSpField00000:${anSpField}")
                                            val simpleName =
                                                ee.simpleName.toString()
                                                    .replace("\$annotations", "")
                                            if (isAllVariables(simpleName, ee)) {
                                                val fieldName = getFieldName(simpleName)
                                                anSpFieldMap[fieldName] = anSpField
//                                                    log("anSpField111111:${anSpFieldMap}")
                                            }
                                        }
                                }
                            }

                        }
                    }

                }

                //将set get 方法转换成全局变量，并添加
                addSpField(spMethodToFieldMap, anSpFieldMap, spClass)


                val s = StringBuilder()
                spClass.createClassStart(s)
                spClass.fieldList.values.forEach { sp ->
                    spClass.createFieldString(sp, s)
//                    log(spClass.createFieldString(sp))
                }
                spClass.createClassEnd(s)

                val file =
                    File(generated + File.separator + spClass.packageName + File.separator + spClass.className + ".kt")
                val parentFile = file.parentFile
                if (!parentFile.exists()) {
                    parentFile.mkdirs()
                }
                if (!file.exists()) {
                    file.createNewFile()
                }
//                log("file:${file.absolutePath}")
//                System.out.println(s)
                setTextFromFile(file, s.toString())

            }
        }
    }

    /**
     * 创建Sp类文件所需要的完成数据
     */
    private fun createSPClass(c: TypeElement): SPClass {
        val packageE = c.enclosingElement as PackageElement
        val sharedPref = c.getAnnotation(AnSharedPref::class.java)
        val spName: String = if (sharedPref.spName.isNotEmpty()) {
            sharedPref.spName
        } else {
            c.simpleName.toString()
        }
        return SPClass(
            packageE.qualifiedName.toString(),
            c.qualifiedName.toString(),
            "${c.simpleName}SpImpl",
            spName
        )
    }

    /**
     * 添加全局变量
     */
    private fun addSpField(ve: VariableElement, spClass: SPClass) {
        val anSpField = ve.getAnnotation(AnSpField::class.java)
        anSpField?.let {
            val fieldClassName = if (anSpField.className.isEmpty()) {
                ve.asType().toString().replace("Object", "Any")
            } else {
                anSpField.className
            }
            spClass.crateSpField(
                fieldClassName = fieldClassName,
                fieldName = ve.simpleName.toString(),
                default = it.defaultValue,
                keyName = if (it.name.isEmpty()) ve.simpleName.toString() else it.name,
                false
            )
        } ?: run {
            val default = ve.constantValue ?: ""
            spClass.crateSpField(
                fieldClassName = ve.asType().toString().replace("Object", "Any"),
                fieldName = ve.simpleName.toString(),
                default = "$default",
                keyName = ve.simpleName.toString(),
                false
            )
        }
    }


    /**
     * 将set get 方法转换成全局变量，并添加
     */
    private fun addSpField(
        spMethodToFieldMap: HashMap<String, SpMethodToField>,
        anSpFieldMap: HashMap<String, AnSpField>,
        spClass: SPClass
    ) {

        spMethodToFieldMap.values.forEach { methodToField ->
            if (methodToField.setField != null && methodToField.getField != null
                && methodToField.setField?.parameter == methodToField.getField?.key
            ) {
                val anSpField = anSpFieldMap[methodToField.getField?.key ?: ""]
                val default = if (anSpField != null && anSpField.defaultValue.isNotEmpty()) {
                    anSpField.defaultValue
                } else {
                    methodToField.default
                }
                val keyName = if (anSpField != null && anSpField.name.isNotEmpty()) {
                    anSpField.name
                } else {
                    methodToField.keyName
                }
                val fieldClassName = if (anSpField == null || anSpField.className.isEmpty()) {
                    (methodToField.getField?.className ?: "").replace("Object", "Any")
                } else {
                    anSpField.className
                }
                spClass.crateSpField(
                    fieldClassName,
                    methodToField.getField?.returnType ?: "",
                    default,
                    keyName, true
                )

            }
        }
    }

    /**
     * 收集set get 方法信息，为了将该方法融合为全局变量做准备
     * @param spMethodToFieldMap 全局变量
     */
    private fun addSpMethodToField(
        ee: ExecutableElement,
        spMethodToFieldMap: HashMap<String, SpMethodToField>
    ) {
        val simpleName = ee.simpleName.toString()
        if (isAllVariables(simpleName, ee)) {
            //从方法中去掉"set"和"get",首字母小写，从而得到全局变量
            val fieldName = getFieldName(simpleName)
            //查看该全局变量是否已经做过处理
            if (!spMethodToFieldMap.contains(fieldName)) {
                spMethodToFieldMap[fieldName] = SpMethodToField()
            }
            when {
                simpleName.indexOf("set") == 0 -> {
                    val className =ee.parameters[0]!!.asType().toString()
//                    val iclass = Class.forName(ee.parameters[0]!!.asType().toString())
//                    if(iclass.isInstance(iclass)){
//                        throw Exception("className：${className}:接口，无法作为数据转化参考")
//                    }else {
//                        iclass.declaredFields.forEach { field->
//                            if(field.declaringClass.isInterface){
//                                throw Exception("className：${className}:类中引用了接口为参数，(${field.declaringClass.canonicalName})接口无法作为数据转化参考")
//                            }
//                        }
//                    }
//                    val className = Class.forName(ee.parameters[0]!!.asType().toString())
//                    log("aaaaaaaaaaaaaaaaaaaaaaaa:${ee.parameters[0].kind.declaringClass}     ${
//                        ee.parameters[0].kind}      ${
//                            ee.parameters[0].kind.isInterface}   ${ee.parameters[0].kind.isField
//                            }    ${ee.parameters[0].kind.isClass}   ${ee.parameters[0].kind.declaringClass.isInterface}")
                    spMethodToFieldMap[fieldName]?.createSetField(
                        fieldName,
                        className
                    )
                }
                simpleName.indexOf("get") == 0 -> {
                    spMethodToFieldMap[fieldName]?.createGetField(
                        fieldName,
                        fieldName,
                        ee.returnType.toString()
                    )
                }
                simpleName.indexOf("is") == 0 -> {
                    spMethodToFieldMap[fieldName]?.createGetField(
                        fieldName,
                        simpleName,
                        ee.returnType.toString()
                    )
                }
            }
            val anSpField = ee.getAnnotation(AnSpField::class.java)
//            log("AnSpField:${ee}${anSpField}")
            if (anSpField != null) {
                spMethodToFieldMap[fieldName]?.default = anSpField.defaultValue
                if (anSpField.name.isNotEmpty()) {
                    spMethodToFieldMap[fieldName]?.keyName = anSpField.name
                }
            }
        }
    }

    /**
     * 判断是否是全局变量
     */
    private fun isAllVariables(simpleName: String, ee: ExecutableElement): Boolean =
        simpleName.length >= 4 && ((simpleName.indexOf("set") == 0 && ee.parameters?.size ?: 0 == 1)
                || (simpleName.indexOf("get") == 0)) || (simpleName.indexOf("is") == 0)

    private fun getFieldName(simpleName: String): String {
        //从方法中去掉"set"和"get",首字母小写，从而得到全局变量
        return if (simpleName.indexOf("is") == 0) {
            if (simpleName.length == 3) simpleName.substring(2, 3).toLowerCase(Locale.CANADA)
            else
                simpleName.substring(2, 3).toLowerCase(Locale.CANADA) + simpleName.subSequence(
                    3,
                    simpleName.length
                ).toString()
        } else {
            if (simpleName.length == 4) simpleName.substring(3, 4).toLowerCase(Locale.CANADA)
            else
                simpleName.substring(3, 4).toLowerCase(Locale.CANADA) + simpleName.subSequence(
                    4,
                    simpleName.length
                ).toString()
        }
    }


    @Throws(IOException::class)
    fun setTextFromFile(file: File, text: String) {
        val pf = file.parentFile
        if (!pf!!.exists()) {
            pf.mkdirs()
        }
        if (!file.exists()) {
            try {
                file.createNewFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
        FileWriter(file).use {
            it.append(text)
            it.flush()
        }
    }


    fun log(content: String) {
        System.out.println("------>${content}")
    }


}