package com.zona.pos.lib_sp_processor.sp


/**
 * @ClassName SpField
 * @Description sp的参数
 * @Author zona
 * @Date 2021/4/1 10:21
 * @Version 1.0
 */
class SpField(
    /**
     * 参数的类名
     */
    val className: String,

    /**
     * 参数名
     */
    val fieldName: String,
    /**
     * 默认值
     */
    val default: String,
    /**
     * 基础变量
     */
    val keyName: String,

    val isOverride:Boolean
)