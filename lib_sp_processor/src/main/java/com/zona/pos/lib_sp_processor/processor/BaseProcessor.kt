package com.zona.pos.lib_sp_processor.processor

import java.util.*
import javax.annotation.processing.AbstractProcessor
import javax.annotation.processing.Filer
import javax.annotation.processing.ProcessingEnvironment
import javax.annotation.processing.RoundEnvironment
import javax.lang.model.SourceVersion
import javax.lang.model.element.TypeElement
import javax.lang.model.util.Elements
import javax.lang.model.util.Types


/**
 * @ClassName BaseProcessor
 * @Description 注解监听基类
 * @Author zona
 * @Date 2021/3/31 15:43
 * @Version 1.0
 */
abstract class BaseProcessor : AbstractProcessor() {
    protected var mFiler: Filer? = null
    protected var elementUtils: Elements? = null
    protected var types: Types? = null

    override fun init(p0: ProcessingEnvironment?) {
        super.init(p0)
        mFiler = processingEnv.filer
        types = processingEnv.typeUtils
        elementUtils = processingEnv.elementUtils
    }


    override fun getSupportedSourceVersion(): SourceVersion? {
        return SourceVersion.latestSupported()
    }

}