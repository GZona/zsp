# Kotlin Sp框架

## 摘要

[点击查看最新版本](https://jitpack.io/#com.gitee.GZona/zsp)

框架优点：  

    1、保持本地与缓存的数据同步 

    2、崩溃后可以对缓存数据进行还原 

    3、操作简单，以接口的形式存储数据，增加数据的可读性 

    4、可对缓存数据进行第一次加工 

    5、业务相关的代码与功能框架完全隔离,当需要更新框架直接修改配置就可以




* [一 初始化配置](#一-初始化配置)
* [二 注解相关](#二-注解相关)
* [三 框架的使用](#三-框架的使用)

### 一 初始化配置

1. 在模块的build.gradle文件中添加Sp框架

~~~~~~

//1、将它添加到存储库末尾的根 build.gradle 中
    allprojects {
		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}
	
...................

//2、在模块对于的build.gradle 中

plugins {
    id 'com.android.application'
    id 'kotlin-android'
    //1、支持kapt，这一步非常重要
    id "kotlin-kapt"
}


...................

//3、Java语音版本支持
 compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = '1.8'
    }
    
...................  
//4、导入库
    
    dependencies{
        //3、导入开发jar包
        //SP工具框架
        //用MMKV本地缓存
        implementation 'com.gitee.GZona.zsp:lib_mmkv:v1.1.04'
        //用系统自带的本地缓存SP存储数据（与MMKV只需要实现一个就可以）
        //implementation 'com.gitee.GZona.zsp:lib_sp:v1.1.04'
    
        //SP的注解框架
        implementation 'com.gitee.GZona.zsp:lib_sp_annotation:v1.1.04'
        //SP的注解解析框架
        kapt 'com.gitee.GZona.zsp:lib_sp_annotation:v1.1.04'
    }
    
    
...................
~~~~~~

2. 在代码中初始化SP框架

~~~~~~


class App : Application() {
    override fun onCreate() {
        super.onCreate()
        //由于SP需要使用到context,所以在使用前需要提前需要初始化
          SharePrefHolder.init { this }
    }
}

~~~~~~

### 二 注解相关

1. AnSharedPref:作用于接口上，代表当前接口与SP关联，

- spName：SP文件名，如果不填SP文件名默认使用接口的接口名


2. AnSpField：作用在全局变量上

- name：SP的键值，不填默认用全局变量的参数名
- defaultValue：SP的全局变量的默认值

3. 应用示例

~~~~~~~


package com.zona.yhsp

import android.text.TextPaint
import android.widget.TextView
import com.zona.lib_sp.SharePrefHolder
import com.zona.lib_sp_annotation.AnSharedPref
import com.zona.lib_sp_annotation.AnSpField


val TestPref: ITest by lazy { SharePrefHolder.getSpClass(ITest::class.java) }

/**
 * @ClassName Test
 * @Description
 * @Author zona
 * @Date 2021/3/31 16:44
 * @Version 1.0
 */
@AnSharedPref()
interface ITest {
    var test1: String

    @AnSpField( defaultValue = "添加的默认数据")
    var test8: String
    var test2: Int
    var test3: Float
    var test4: Double
    var test5: Boolean
    var test6: List<TextView>?
    var test7: List<Map<String, HashMap<Int, TextPaint>>>?
    var test8: Test?

    fun d(i: Int): String {
        return "测试数据：${i}"
    }

    fun text(): String {
        return test1
    }

}

~~~~~~

class Test(val ddd:String) {
    var texs1: String? = null
    var texs2: String = ""
    var aaaa: Int = 0
}

~~~~~~


~~~~~~~

### 三 框架的使用

应用示例

~~~~~~~

setContentView(R.layout.layout_test)
        val content1 = findViewById<TextView>(R.id.content1)
        val content2 = findViewById<TextView>(R.id.content2)
        val btn1_1 = findViewById<TextView>(R.id.btn1_1)
        val btn1_2 = findViewById<TextView>(R.id.btn1_2)
        val btn2_1 = findViewById<TextView>(R.id.btn2_1)
        val btn2_2 = findViewById<TextView>(R.id.btn2_2)
        content1.text = TestPref.test1
        content2.text = TestPref.test8
        btn1_1.setOnClickListener {
            content1.text = btn1_1.text
            TestPref.test1 = btn1_1.text.toString()
        }
        btn1_2.setOnClickListener {
            content1.text = btn1_2.text
            TestPref.test1 = btn1_2.text.toString()
            Toast.makeText(this, TestPref.text(), Toast.LENGTH_SHORT).show()
        }
        
        TestPref:test8 = Test("测试")
        btn2_2.setOnClickListener {
            //如果需要修改对象中的数据，可以使用该方法同步
            TestPref:test8.saveSpData {
                DomeConf.test?.aaaa = (DomeConf.test?.aaaa ?: 0) + 1
            }
        }

~~~~~~~




### 四 框架迭代历史

- v1.1.00:第一个正式版本
- v1.1.03:解决了存储接口数据，为空的情况下无法获取数据的问题
- v1.1.04:解决了存储接口数据，接口对应的实现类使用SerializedName别名时无法生成数据的BUG
