package com.zona.lib_sp

import android.content.Context
import com.tencent.mmkv.MMKV
import java.lang.Exception


/**
 * @ClassName SharePrefHolder
 * @Description
 * @Author zona
 * @Date 2021/4/1 15:23
 * @Version 1.0
 */
object SharePrefHolder {

    private var application: (() -> Context)? = null
    fun init(application: (() -> Context)) {
        this.application = application
        try {
            MMKV.initialize(getContext());
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getContext(): Context {
        return application?.invoke()!!
    }

    fun <T> getSpClass(clazz: Class<T>): T {
        val class1 = Class.forName("${clazz.canonicalName}SpImpl")
        return class1.newInstance() as T
    }

}
