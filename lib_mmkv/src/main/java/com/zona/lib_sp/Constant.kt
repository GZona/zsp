package com.zona.lib_sp


/**
 * @ClassName Constant
 * @Description 常量
 * @Author zona
 * @Date 2021/4/2 16:13
 * @Version 1.0
 */
object Constant {
    const val SP_PACKAGE_NAME = "com.zona.lib_mmkv"
}