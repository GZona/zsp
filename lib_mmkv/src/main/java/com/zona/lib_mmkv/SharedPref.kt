package com.zona.lib_mmkv

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.tencent.mmkv.MMKV
import com.zona.lib_sp.SharePrefHolder
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * Create by LiJie at 2019-05-29
 * 通过代理从[MMKV]获取或保存变量
 * var account by SharedPref(default = "")
 */
class SharedPref<T>(
    private val default: T,
    private val keyName: String = "",
    spName: String = "shared_pref",
    context: Context = SharePrefHolder.getContext()
) : ReadWriteProperty<Any, T> {


    private val sharedPreferences: MMKV? by lazy {
        MMKV.mmkvWithID(spName, MMKV.MULTI_PROCESS_MODE)
    }

    //    fun d(){
//        val MMKV = MMKV.mmkvWithID("myData")
//    }
//        context.getSharedPreferences(spName, Context.MODE_PRIVATE)
//    private val sharedPreferences: SharedPreferences =
//        context.getSharedPreferences(spName, Context.MODE_PRIVATE)

    override fun getValue(thisRef: Any, property: KProperty<*>): T {
        val name = if (keyName.isEmpty()) property.name else keyName
        val mmkv = sharedPreferences ?: throw IllegalArgumentException("MMKV is Null")
        return with(mmkv) {
            @Suppress("IMPLICIT_CAST_TO_ANY", "UNCHECKED_CAST")
            when (default) {
                is String -> getString(name, default) as T
                is Int -> getInt(name, default) as T
                is Float -> getFloat(name, default) as T
                is Boolean -> getBoolean(name, default) as T
                is Long -> getLong(name, default) as T
                is Double -> decodeDouble(name, default) as T
                else -> throw IllegalArgumentException("not support type")
            }
        }
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: T) {
        val name = if (keyName.isEmpty()) property.name else keyName
        val mmkv = sharedPreferences ?: throw IllegalArgumentException("MMKV is Null")
//        Log.e("nadyboy", "name:${name}   value:${value}")
//        mmkv.removeValueForKey(name)
        with(mmkv) {
            when (value) {
                is String -> putString(name, value)
                is Int -> putInt(name, value)
                is Float -> putFloat(name, value)
                is Boolean -> putBoolean(name, value)
                is Long -> putLong(name, value)
                is Double -> encode(name, value)
                else -> throw IllegalArgumentException("not support type")
            }
        }
    }
}