package com.zona.dome_sp

import com.zona.lib_sp_annotation.AnSharedPref
import com.zona.lib_sp_annotation.lazySp


val DomeConf by lazySp<IDomeConf>()

/**
 * @ClassName DomeConf
 * @Description
 * @Author zona
 * @Date 2021/4/20 16:06
 * @Version 1.0
 */
@AnSharedPref
interface IDomeConf {


    var test: Test?
}