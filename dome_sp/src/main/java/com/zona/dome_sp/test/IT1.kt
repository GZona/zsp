package com.zona.dome_sp.test

/**
 *
 * @author: zona
 * @date: 2021/5/31
 */
interface IT1 {
    val b:Float
    val c:Double
    val a:Int
    val d:String
    val list:List<IT2>
}