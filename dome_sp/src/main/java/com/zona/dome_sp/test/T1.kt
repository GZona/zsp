package com.zona.dome_sp.test

/**
 *
 * @author: zona
 * @date: 2021/5/31
 */
class T1(
    override val b: Float,
    override val c: Double,
    override val a: Int,
    override val d: String, override val list: List<IT2>
) : IT1 {
}