package com.zona.dome_sp

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.*
import com.zona.lib_sp_annotation.saveSpData

class MainActivity : AppCompatActivity() {
    var index: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (DomeConf.test == null) {
            DomeConf.test = Test("qweer")
        }
        findViewById<View>(R.id.test).setOnClickListener {
            DomeConf::test.saveSpData {
                DomeConf.test?.aaaa = (DomeConf.test?.aaaa ?: 0) + 1
            }
            Log.e("nadyboy", "DomeConf.test:${Gson().toJson(DomeConf.test)}")
        }
    }


}