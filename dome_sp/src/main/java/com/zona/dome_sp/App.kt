package com.zona.dome_sp

import android.app.Application
import com.zona.lib_sp.SharePrefHolder

/**
 * @ClassName App
 * @Description
 * @Author zona
 * @Date 2021/4/20 16:12
 * @Version 1.0
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        SharePrefHolder.init { this }
    }
}