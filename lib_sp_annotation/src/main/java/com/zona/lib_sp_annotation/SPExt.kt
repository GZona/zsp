package com.zona.lib_sp_annotation

import kotlin.reflect.KMutableProperty
import kotlin.reflect.KMutableProperty0
import kotlin.reflect.KProperty

/**
 * SP 会使用的到扩展函数
 * @author: zona
 * @date: 2021/7/26
 */


fun <T : Any?> KMutableProperty0<T>.saveSpData(update: (T) -> Unit) {
    val t = this.invoke()
    update.invoke(t)
    this.set(t)
}



inline fun <reified T : @AnSharedPref Any> Class<T>.getSpClass(): T {
    val class1 = Class.forName("${this.canonicalName}SpImpl")
    return class1.getDeclaredConstructor().newInstance() as T
}


inline fun <reified T : @AnSharedPref Any> lazySp(): Lazy<T> = lazy { T::class.java.getSpClass() }