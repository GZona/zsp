package com.zona.lib_sp_annotation


/**
 * @ClassName AnSpField
 * @Description SP的全局参数注解，用来添加默认值，和默认
 * @Author zona
 * @Date 2021/4/1 10:36
 * @Version 1.0
 */

@Target(
        /**
         * 类或接口
         */
        AnnotationTarget.ANNOTATION_CLASS,
        AnnotationTarget.FIELD,
        AnnotationTarget.TYPE, AnnotationTarget.PROPERTY,
)
@Retention(
        /**
         * 不生成在class文件中
         */
        AnnotationRetention.SOURCE
)
annotation class AnSpField(
        /**
         * sp的key值
         */
        val name: String = "",
        /**
         * 默认值
         */
        val defaultValue: String = "",
        /**
         * 参数的Class,由于生成的方式是按照JAVA格式生成的，所以可能会出现不如意的一些值，而该值就是为了解决这个问题的
         */
        val className: String = "") {
}