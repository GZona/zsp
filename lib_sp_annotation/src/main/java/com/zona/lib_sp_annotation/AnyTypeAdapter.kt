package com.zona.lib_sp_annotation

import com.google.gson.Gson
import com.google.gson.TypeAdapter
import com.google.gson.annotations.SerializedName
import com.google.gson.internal.LinkedTreeMap
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import java.io.StringReader


/**
 *
 * @author: zona
 * @date: 2021/10/26
 */
class AnyTypeAdapter<T : Any?>(
    val josn: Gson,
    val map: MutableMap<String, MutableList<String>>,
    val type: TypeToken<T>,
) : TypeAdapter<T>() {


    override fun write(out: JsonWriter?, value: T) {
        if (value != null && out != null) {
            write(value, out)
        }else{
            out?.nullValue()
        }
    }

    fun write(a: Any, out: JsonWriter) {
        val clazz = a::class.java
        var list = map[type.type.toString()]
        if (list == null) {
            list = ArrayList()
            map[type.type.toString()] = list
        }
        if (!list.contains(clazz.name)) {
            list.add(clazz.name)
        }
        try {
            josn.getAdapter(clazz as Class<T>).write(out, a as T)
        } catch (e: Exception) {
            e.printStackTrace()
            try {
                System.out.println("${clazz}  ${Gson().toJson(a)}")
            } catch (e: Exception) {
            }
        }
    }

    override fun read(input: JsonReader?): T? {
        if (input == null) {
            return null
        }
        val list = map[type.type.toString()]
        val item = readJson(input)
        val jsonString = josn.toJson(item)
        list?.forEach { clazzName ->
            try {
                val clazz = Class.forName(clazzName)
                if (item is MutableMap<*, *>) {
                    val list1 = ArrayList(item.keys)
                    var isRead = true
                    val end = if (list1.size > 10) 10 else list1.size
                    try {
                        for (index in 0 until end) {
                            getDeclaredField(clazz, list1[index].toString())
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                        isRead = false
                    }
                    if (isRead) {
                        val jsonReader = JsonReader(StringReader(jsonString))
                        return (josn.getAdapter(clazz).read(jsonReader) as T?)
                    }
                } else {
                    val jsonReader = JsonReader(StringReader(jsonString))
                    return (josn.getAdapter(clazz).read(jsonReader) as T?)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        throw  Exception("读数据时，抽象接口(${type.type})没有找到合适的类做转换(已提供的转换类列表:${list})")
    }

    private fun getDeclaredField(clazz: Class<*>, data: String) {
        try {
            clazz.getDeclaredField(data)
        } catch (e: Exception) {
//            e.printStackTrace()
            //如果没有该变量，可能变量名是通过SerializedName注解修改
            clazz.declaredFields.forEach { field ->
                field.declaredAnnotations.forEach { annotation ->
                    if (annotation is SerializedName) {
                        if (data == annotation.value) {
                            return
                        }
                    }
                }
            }
            val c = clazz.superclass
            if (c != null && c.canonicalName != Object::class.java.canonicalName
                && c.canonicalName != Any::class.java.canonicalName) {
                getDeclaredField(c, data)
            } else {
                throw e
            }
        }

    }

    private fun readJson(input: JsonReader): Any? {
        val token: JsonToken = input.peek()
        return when (token) {
            JsonToken.BEGIN_ARRAY -> {
                val list: MutableList<Any> = java.util.ArrayList()
                input.beginArray()
                while (input.hasNext()) {
                    list.add(readJson(input)!!)
                }
                input.endArray()
                list
            }
            JsonToken.BEGIN_OBJECT -> {
                val map: MutableMap<String, Any?> = LinkedTreeMap()
                input.beginObject()
                while (input.hasNext()) {
                    val name = input.nextName()
                    val nextString = readJson(input)
                    map[name] = nextString
                }
                input.endObject()
                map
            }
            JsonToken.STRING -> input.nextString()
            JsonToken.NUMBER -> input.nextDouble()
            JsonToken.BOOLEAN -> input.nextBoolean()
            JsonToken.NULL -> {
                input.nextNull()
                null
            }
            else -> throw IllegalStateException()
        }
    }


}