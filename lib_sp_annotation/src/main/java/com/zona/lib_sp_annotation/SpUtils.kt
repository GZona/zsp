package com.zona.lib_sp_annotation

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

/**
 * Created by jeri on 2021/2/26.
 */
object SpUtils {
    inline fun <reified T : Any> serialize(
            spName: String,
            t: T?,
            type: TypeToken<T>,
    ): SpSerializeData? {
        if (t == null) {
            return null
        }
        try {
            val map = HashMap<String, MutableList<String>>()
            initMap(type, t, map)
//            System.out.println("nadyboy initMap:${map}")
            val json = toJson(t, map)
            return SpSerializeData(json, GsonBuilder().disableHtmlEscaping().create().toJson(map))
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

    fun initMap(type: TypeToken<*>, t: Any?, map: HashMap<String, MutableList<String>>) {
        if (type.rawType.isInterface) {
            if (!List::class.java.isAssignableFrom(type.rawType)
                    && !Map::class.java.isAssignableFrom(type.rawType)
                    && !Collection::class.java.isAssignableFrom(type.rawType)
            ) {
                var list = map[type.type.toString()]
                if (list == null) {
                    list = ArrayList()
                    map[type.type.toString()] = list
                }
                if (t != null) {
                    val clazz = t::class.java.name
                    if (!list.contains(clazz)) {
                        list.add(clazz)
                    }
                }
            } else {
                if (List::class.java.isAssignableFrom(type.rawType)) {
                    val ty = type.type
                    if (ty is ParameterizedType) {
                        val typeArguments = ty.actualTypeArguments
                        if (typeArguments.isNotEmpty() && t is List<Any?>) {
                            val typeToken = TypeToken.get(typeArguments[0])
                            t.forEach { a ->
                                initMap(typeToken, a, map)
                            }
                        }
                    }
                } else if (Map::class.java.isAssignableFrom(type.rawType)) {
                    val ty = type.type
                    if (ty is ParameterizedType) {
                        val typeArguments = ty.actualTypeArguments
                        if (typeArguments.size >= 2 && t is Map<*, *>) {
                            var typeToken = TypeToken.get(typeArguments[0])
                            if (typeToken.rawType.isInterface) {
                                t.keys.forEach { a ->
                                    initMap(typeToken, a, map)
                                }
                            }
                            typeToken = TypeToken.get(typeArguments[1])
                            if (typeToken.rawType.isInterface) {
                                t.values.forEach { a ->
                                    initMap(typeToken, a, map)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    inline fun <reified T> deSerialization(str: String, clazzList: String, typeToken: Type): T? {
        if (str.isEmpty()) return null
        try {
            val list: MutableMap<String, MutableList<String>> = HashMap()
            try {
                if (clazzList.isNotEmpty()) {
                    val lista: MutableMap<String, MutableList<String>> =
                            GsonBuilder().disableHtmlEscaping().create()
                                    .fromJson(clazzList,
                                            object :
                                                    TypeToken<MutableMap<String, MutableList<String>>>() {}.type)
                    list.putAll(lista)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return createGson(list).fromJson<T?>(str, typeToken)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

//    fun createGson(
//        oldList: List<String> = arrayListOf(),
//        list: MutableList<String> = arrayListOf(),
//    ): Gson {
//        val gsonBuilder = GsonBuilder()
//        return gsonBuilder.registerTypeAdapterFactory(
//            SPTypeAdapterFactory(
//                oldList,
//                list
//            )
//        ).create()
//    }

    fun createGson(
            map: MutableMap<String, MutableList<String>>,
    ): Gson {
        val gsonBuilder = GsonBuilder()
        return gsonBuilder.registerTypeAdapterFactory(
                SPAnyTypeAdapterFactory(map)
        ).create()
    }

    inline fun <reified T : Any> toJson(
            any: T?,
            map: MutableMap<String, MutableList<String>>,
    ): String {
        return createGson(map).toJson(any).also {
//            if (it.length > 4000) {
//                var i = 0
//                while (i < it.length) {
//                    if (i + 4000 < it.length)
//                        System.out.println("nadyboyJ:   ${it.substring(i, i + 4000)}")
//                    else
//                        System.out.println("nadyboyJ:   ${it.substring(i, it.length)}")
//                    i += 4000
//                }
//            } else
//                System.out.println("nadyboyJ:${it}")
//
//            System.out.println("nadyboy   ${Gson().toJson(map)}")
        }
    }


}

