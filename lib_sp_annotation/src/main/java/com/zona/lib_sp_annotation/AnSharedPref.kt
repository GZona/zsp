package com.zona.lib_sp_annotation


//@Target(ElementType.TYPE)	用于接口(注解本质上也是接口),类,枚举
//@Target(ElementType.FIELD)	用于字段,枚举常量
//@Target(ElementType.METHOD)	用于方法
//@Target(ElementType.PARAMETER)	用于方法参数
//@Target(ElementType.CONSTRUCTOR)	用于构造参数
//@Target(ElementType.LOCAL_VARIABLE)	用于局部变量
//@Target(ElementType.ANNOTATION_TYPE)	用于注解
//@Target(ElementType.PACKAGE)	用于包

/**
 * @ClassName SharedPref
 * @Description
 * @Author zona
 * @Date 2021/3/31 15:04
 * @Version 1.0
 */
@Target(
    /**
     * 类或接口
     */
    AnnotationTarget.CLASS,
    AnnotationTarget.ANNOTATION_CLASS,
    AnnotationTarget.FIELD,
    AnnotationTarget.TYPE
)
@Retention(
    /**
     * 不生成在class文件中
     */
    AnnotationRetention.SOURCE
)
annotation class AnSharedPref(val spName: String="")
