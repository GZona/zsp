package com.zona.lib_sp_annotation

import com.google.gson.Gson
import com.google.gson.TypeAdapter
import com.google.gson.TypeAdapterFactory
import com.google.gson.reflect.TypeToken

/**
 *
 * @author: zona
 * @date: 2021/10/26
 */
class SPAnyTypeAdapterFactory(val map: MutableMap<String, MutableList<String>>) :
    TypeAdapterFactory {
    override fun <T : Any?> create(gson: Gson?, type: TypeToken<T>?): TypeAdapter<T>? {
        if (type?.rawType?.isInterface == true && gson != null) {
            if (!List::class.java.isAssignableFrom(type.rawType)
                && !Map::class.java.isAssignableFrom(type.rawType)
                && !Collection::class.java.isAssignableFrom(type.rawType)
            ) {
                return AnyTypeAdapter(gson, map, type)
            }
        }
        return null
    }
}